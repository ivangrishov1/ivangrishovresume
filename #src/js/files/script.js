/// vanilla-tilt
document.addEventListener("DOMContentLoaded", function () {
    let list = document.querySelectorAll('.social__list li');
    let bg = document.querySelector('.contacts__container');

    VanillaTilt.init(document.querySelectorAll(".social__list li a"), {
        max: 30,
        speed: 400,
        glare: true,
        "max-glare": 1,
    });
})
